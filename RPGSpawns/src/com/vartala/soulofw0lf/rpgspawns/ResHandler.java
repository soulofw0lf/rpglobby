package com.vartala.soulofw0lf.rpgspawns;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ResHandler implements CommandExecutor {
	RPGSpawns Rpgs;
	public ResHandler(RPGSpawns rpgs){
		this.Rpgs = rpgs;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		Player player = (Player) sender;
		Player p = Bukkit.getPlayer(args[0]);
		Location loc = player.getLocation();
		if ((this.Rpgs.getConfig().getBoolean("AllowRes")) == true){
			if (!(player.hasPermission("spawns.res"))){
				player.sendMessage("You do not have permission to use this command");
				return true;
			}
			if (args.length != 1){
				player.sendMessage("Improper usage, please use /res playername");
				return true;
			}
			if (p == null) {
				sender.sendMessage("Could Not Find Player!!");
				return true;
			}
			double resdist = player.getLocation().distance(p.getLocation());

			if (resdist <= 30){
				Double x = loc.getX();
				Double y = loc.getY();
				Double z = loc.getZ();
				Float pitch = loc.getPitch();
				Float yaw = loc.getYaw();
				if (!(this.Rpgs.getConfig().contains(p.getName()))){
					this.Rpgs.getConfig().set(p.getName() + ".X", x);
					this.Rpgs.getConfig().set(p.getName() + ".Y", y);
					this.Rpgs.getConfig().set(p.getName() + ".Z", z);
					this.Rpgs.getConfig().set(p.getName() + ".Pitch", pitch);
					this.Rpgs.getConfig().set(p.getName() + ".Yaw", yaw);
					this.Rpgs.getConfig().set(p.getName() + ".rezzer", player.getName());
					this.Rpgs.saveConfig();
					player.sendMessage("you are trying to ressurect " + p.getName());
					return true;
				} else {
					player.sendMessage("this player is already being ressurected!");
					return true;
				}
			} else {
				player.sendMessage("you are too far away to ressurct this player!");
				return true;
			}
		} else {
			player.sendMessage("Ressurecting is disabled on this server!");
			return true;
		}
	}
}
