package com.vartala.soulofw0lf.rpgspawns;

import java.util.Set;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SpawnHandler implements CommandExecutor {
	RPGSpawns Rpgs;
	public SpawnHandler(RPGSpawns rpgs){
		this.Rpgs = rpgs;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		Player player = (Player) sender;
		Location loc = player.getLocation();
		if (args.length <= 0){
			player.sendMessage("incorrect command format! you must use /spawns (add,del,tp,list)");
			return true;
		}
		if ((!(args[0].equalsIgnoreCase("list"))) && (!(args[0].equalsIgnoreCase("del"))) && (!(args[0].equalsIgnoreCase("tp"))) && (!(args[0].equalsIgnoreCase("add")))){
			player.sendMessage("incorrect command format! you must use /spawns (add,del,tp,list)");
			return true;
		}
		if (args[0].equalsIgnoreCase("list")){
			if (!(player.hasPermission("spawns.list"))){
			player.sendMessage("You do not have permission to use this command");
			return true;
			}
			if (!(this.Rpgs.getConfig().contains(loc.getWorld().getName()))){
				player.sendMessage("There are no spawns on this world yet! please use (/spawns add {spawnname}) to add a spawn here!");
				return true;
			}
			final Set<String> keys = this.Rpgs.getConfig().getConfigurationSection(loc.getWorld().getName()).getKeys(false);
			if (keys.size() <= 0) {
				player.sendMessage("There are no spawn points on this world");
				return true;
			}
			for (String key : this.Rpgs.getConfig().getConfigurationSection(loc.getWorld().getName()).getKeys(false)){
				
				player.sendMessage(key + "\n");
			}
			return true;
		}
		if (args[0].equalsIgnoreCase("tp")){
			if (!(player.hasPermission("spawns.tp"))){
				player.sendMessage("You do not have permission to use this command");
				return true;
				}
			if (args.length != 2){
				player.sendMessage("proper usage: /spawns tp spawnname [Note this command must be used on the world the spawn is located on]");
				return true;
			}
			if (!(this.Rpgs.getConfig().getConfigurationSection(loc.getWorld().getName()).contains(args[1]))){
				player.sendMessage(args[1] + " does not exist on " + loc.getWorld().getName());
				return true;
			}
			World thisworld = loc.getWorld();
			String world = thisworld.getName();
			String pitch = this.Rpgs.getConfig().getString(world + "." + args[1] + ".Pitch");
			String yaw = this.Rpgs.getConfig().getString(world + "." + args[1] + ".Yaw");
			Float newpitch = Float.parseFloat(pitch);
			Float newyaw = Float.parseFloat(yaw);
			Location newloc = new Location(thisworld,this.Rpgs.getConfig().getDouble(world + "." + args[1] + ".X"),this.Rpgs.getConfig().getDouble(world + "." + args[1] + ".Y"),this.Rpgs.getConfig().getDouble(world + "." + args[1] + ".Z"),newyaw,newpitch);
			player.teleport(newloc);
			return true;
		}
		if (args[0].equalsIgnoreCase("del")){
			if (!(player.hasPermission("spawns.del"))){
				player.sendMessage("You do not have permission to use this command");
				return true;
				}
			if (args.length != 2){
				player.sendMessage("Improper deletion use /spawns del name");
				return true;
			}
			if (!(this.Rpgs.getConfig().getConfigurationSection(loc.getWorld().getName()).contains(args[1]))){
				player.sendMessage(args[1] + " does not exist on " + loc.getWorld().getName());
				return true;
			}
			String world = loc.getWorld().getName();
			this.Rpgs.getConfig().getConfigurationSection(world).set(args[1],null);
			this.Rpgs.saveConfig();
			player.sendMessage("spawn point " + args[1] + " on world " + world + " has been removed.");
			return true;
		}
		if (args[0].equalsIgnoreCase("add")){
			if (!(player.hasPermission("spawns.add"))){
				player.sendMessage("You do not have permission to use this command");
				return true;
				}
			String name = args[1];
			if (args.length != 2){
				player.sendMessage("You must enter a name for this spawn");
				return true;
			}
			Double x = loc.getX();
			Double y = loc.getY();
			Double z = loc.getZ();
			Float pitch = loc.getPitch();
			Float yaw = loc.getYaw();
			World world = loc.getWorld();
			String World = world.getName();
			if (!(this.Rpgs.getConfig().contains(World + "." + name))){
			//this.Rpgs.getConfig().createSection(World + name);
			this.Rpgs.getConfig().set(World + "." + name + ".X", x);
			this.Rpgs.getConfig().set(World + "." + name + ".Y", y);
			this.Rpgs.getConfig().set(World + "." + name + ".Z", z);
			this.Rpgs.getConfig().set(World + "." + name + ".Pitch", pitch);
			this.Rpgs.getConfig().set(World + "." + name + ".Yaw", yaw);
			this.Rpgs.saveConfig();
			player.sendMessage("you have succesfully created a new spawn point named �4" + name + "�F on �6" + World + "�Fat:�2" + x + "," + y + "," + z + "�F." );
			return true;
			} else {
				player.sendMessage("A spawn point with that name already exists!");
				return true;
			}
		}
		return false;
	}
}
