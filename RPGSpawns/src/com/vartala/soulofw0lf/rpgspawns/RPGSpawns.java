package com.vartala.soulofw0lf.rpgspawns;

import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public class RPGSpawns extends JavaPlugin implements Listener{

	@Override
	public void onEnable(){
		getCommand("spawns").setExecutor(new SpawnHandler(this));
		getCommand("res").setExecutor(new ResHandler(this));
		getServer().getPluginManager().registerEvents(new RespawnListener(this), this);
		saveDefaultConfig();
		getLogger().info("onEnable has been invoked!");
	}
	public void onDisable(){
		getLogger().info("onDisable has been invoked!");
		saveConfig();
	}
}