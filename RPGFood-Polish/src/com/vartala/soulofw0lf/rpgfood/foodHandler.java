package com.vartala.soulofw0lf.rpgfood;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

public class foodHandler implements CommandExecutor {

	RpgFood Rpgf;

	public foodHandler(RpgFood rpgf) {
		this.Rpgf = rpgf;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		Player player = (Player) sender;
		if (player.hasPermission("food.give") || (!(sender instanceof Player))){
			if (args[0].equalsIgnoreCase("give")){
				Player p = Bukkit.getPlayer(args[1]);
				if (p == null){
					player.sendMessage("Nie mozna Odnalesc gracza");
					return true;
				}
				if (!(this.Rpgf.getConfig().contains("Food." + args[2].replaceAll("_", " ")))){
					player.sendMessage("Taki przedmiot nie istnieje!");
					return true;
				}
				if (args.length <= 3){
					player.sendMessage("Niepoprawna komenda! Poprawna /food give nazwa_gracza nazwa_przedmiotu ilosc!");
					return true;
				}
				PlayerInventory pi = p.getInventory();
				Material additems = Material.getMaterial(this.Rpgf.getConfig().getInt("Food." + args[2].replaceAll("_", " ") + ".ItemID"));
				Integer number = Integer.parseInt(args[3]);
				ItemStack is = new ItemStack(additems, number); 
				ItemMeta im = is.getItemMeta();
				ArrayList<String> lore = new ArrayList<String>();
				lore.add("Leczy " + this.Rpgf.getConfig().getDouble("Food." + args[2].replaceAll("_", " ") + ".health") + "  zdrowia przez " + this.Rpgf.getConfig().getDouble("Food." + args[2].replaceAll("_", " ") + ".time") + "  sekund.");
				im.setLore(lore);
				im.setDisplayName(args[2].replaceAll("_", " "));
				is.setItemMeta(im);
				pi.addItem(is);
				player.sendMessage("Dales " + p.getName() + " " + number + " " + additems.name() + "!");
				return true;
			}
			
		}
		if (player.hasPermission("food.add")){
			if (args[0].equalsIgnoreCase("effect")){
				if (args.length != 5){
					player.sendMessage("Nieprawna komenda, wpisz /food effect nazwa_jedzenia nazwa_efektu czas_trwania poziom_mikstury");
					return true;
				}
				if (!(this.Rpgf.getConfig().contains("Food." + args[1].replaceAll("_", " ")))){
					player.sendMessage("Taki przedmiot nie istnieje!");
				}
				Integer dura = Integer.parseInt(args[3]);
				Integer amp = Integer.parseInt(args[4]);
				this.Rpgf.getConfig().set("Food." + args[1].replaceAll("_", " ") + ".Food Buff", args[2]);
				this.Rpgf.getConfig().set("Food." + args[1].replaceAll("_", " ") + ".Buff Duration", dura);
				this.Rpgf.getConfig().set("Food." + args[1].replaceAll("_", " ") + ".Buff Amp", amp);
				this.Rpgf.saveConfig();
				player.sendMessage("Dodales efekt " + args[2] + " na " + args[3] + " sekund do " + args[1] + "o sile " + args[4]);
				return true;
				
			}

			if (args.length != 4){
				player.sendMessage("Niepoprawna komenda! Wpisz /food id_przedmiotu nazwa_jedzenia ilosc_zycia# czas_jedzenia#");
				return true;
			}
			Integer ItemId = Integer.parseInt(args[0]);
			Double Time = Double.parseDouble(args[3]);
			Integer Health = Integer.parseInt(args[2]);			
			this.Rpgf.getConfig().set("Food." + args[1].replaceAll("_", " ") + ".ItemID", ItemId);
			this.Rpgf.getConfig().set("Food." + args[1].replaceAll("_", " ") + ".health", Health);
			this.Rpgf.getConfig().set("Food." + args[1].replaceAll("_", " ") + ".time", Time);
			this.Rpgf.saveConfig();
			player.sendMessage("Zapisales " + args[1].replaceAll("_", " ") + ", aby odnawial " + args[2] + " punktow zycia, przez " + args[3] + " sekund.");
			return true;	
		} else {
			player.sendMessage("Nie masz pozwolenia na uzywanie tej komendy!");
			return true;
		}
	}
}
