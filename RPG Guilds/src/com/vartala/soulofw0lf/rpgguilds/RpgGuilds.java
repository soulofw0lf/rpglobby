package com.vartala.soulofw0lf.rpgguilds;



import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.Event.Result;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;





public class RpgGuilds extends JavaPlugin implements Listener {
	private RpgGuilds plugin;
	@Override
	public void onEnable(){
		plugin = this;
		getCommand("guild").setExecutor(new GuildHandler(this));
		//done
		getCommand("g").setExecutor(new gcHandler(this));
		getCommand("o").setExecutor(new oHandler(this));
		getCommand("gbank").setExecutor(new gBankHandler(this));
		getCommand("gtp").setExecutor(new gTP(this));
		//done without editable permissions
		getCommand("grank").setExecutor(new gRankHandler(this));
		Bukkit.getPluginManager().registerEvents(this, this);
		getLogger().info("RpgGuilds has been enabled!");
		saveDefaultConfig();
		if (!(getConfig().contains("TP"))){
			getConfig().set("TP", false);
			saveConfig();
		}
	}

	@Override
	public void onDisable(){
		getLogger().info("RpgGuilds has been Disabled!");
		saveConfig();
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void onPlayerClick(InventoryClickEvent event){
		Player player = (Player)event.getWhoClicked();
		if (getConfig().contains(player.getName())){
			String guildn = getConfig().getString(player.getName() + ".Guild.Name");
			String rank = null;
			for (String key : getConfig().getConfigurationSection("Guilds." + guildn + ".Ranks").getKeys(false)){
				if (event.getInventory().getTitle().contains(key)){
					rank = key;
				}
			}
			if (rank != null){
				event.setCancelled(true);
				event.setResult(Result.DENY);
				ItemStack item = event.getCurrentItem();
				if(item == null || item.getTypeId() == 0){
					return;
				} else {
					ItemMeta im = event.getCurrentItem().getItemMeta();
					if (im.hasDisplayName()){
						String iname = im.getDisplayName();
						Short green = 5;
						Short red = 14;
						if (getConfig().getBoolean("Guilds." + guildn + ".Ranks." + rank + "." + iname) == false){
							getConfig().set("Guilds." + guildn + ".Ranks." + rank + "." + iname, true);
							ArrayList<String> lore = new ArrayList<String>();
							lore.add("True");
							im.setLore(lore);
							item.setItemMeta(im);
							item.setDurability(green);
							saveConfig();
						} else {
							getConfig().set("Guilds." + guildn + ".Ranks." + rank + "." + iname, false);
							ArrayList<String> lore = new ArrayList<String>();
							lore.add("False");
							im.setLore(lore);
							item.setItemMeta(im);
							item.setDurability(red);
							saveConfig();
						}
					}
				}
			}
		}
	}
	@EventHandler(priority = EventPriority.HIGH)
	public void onPlayerDmg(EntityDamageByEntityEvent event){
		if(event.getEntity() instanceof Player){
			Player a = (Player)event.getEntity();
			Player b = null;
			if(event.getDamager() instanceof Player)
				b = (Player)event.getEntity();
			if(event.getDamager() instanceof Arrow)
				if (((Arrow)event.getDamager()).getShooter() instanceof Player)
					b = (Player)((Arrow)event.getDamager()).getShooter();
			if (b != null){
				if ((getConfig().contains(a.getName())) && (getConfig().contains(b.getName()))){ 
					String guild = getConfig().getString(a.getName() + ".Guild.Name");
					String guild1 = getConfig().getString(b.getName() + ".Guild.Name");
					if (guild.equalsIgnoreCase(guild1)){
						event.setDamage(0);
						event.setCancelled(true);
					}
				}
			}
		}
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		event.setJoinMessage(null);
		if (getConfig().contains(player.getName())){
			String guild = getConfig().getString(player.getName() + ".Guild.Name");
			for (String key : getConfig().getConfigurationSection("Guilds." + guild + ".Players").getKeys(false)){
				if (Bukkit.getPlayer(key) != null){
					if (Bukkit.getPlayer(key).getName() == player.getName()){

						if (!(getConfig().getString("Guilds." + guild + ".Gmotd") == null)){
							player.sendMessage(getConfig().getString("Guilds." + guild + ".Gmotd"));
						} else {
							player.sendMessage("You are a part of " + guild);
						}
					} else {
						Player p = Bukkit.getPlayer(key);
						p.sendMessage("�3" + player.getName() + "�2 Has come online!");
					}
				}

			}
		}

	}
	@EventHandler
	public void onKick(PlayerKickEvent event2){
		event2.setLeaveMessage(null);
	}
	public void teleport(Player p, Player p2){
		final Player player = p;
		final Player player2 = p2;
		final Location loc = player.getLocation();
		final Location Loc = player2.getLocation();
		player.sendMessage("About teleport, don't move!");
		player2.sendMessage("About teleport, don't move!");
		new BukkitRunnable(){



			int count = 8;
			

			@Override
			public void run(){



				player.sendMessage( "Wait " + count + " Seconds." );
				player2.sendMessage( "Wait " + count + " Seconds." );
				count--;
				if (player.getLocation().getX() != loc.getX()){
					player.sendMessage("Cancelled teleport, don't move!");
					player2.sendMessage("Cancelled teleport, don't move!");
					cancel();
				}
				if (player.getLocation().getZ() != loc.getZ()){
					player.sendMessage("Cancelled teleport, don't move!");
					player2.sendMessage("Cancelled teleport, don't move!");
					cancel();
				}
				if (player2.getLocation().getX() != Loc.getX()){
					player.sendMessage("Cancelled teleport, don't move!");
					player2.sendMessage("Cancelled teleport, don't move!");
					cancel();
				}
				if (player2.getLocation().getZ() != Loc.getZ()){
					player.sendMessage("Cancelled teleport, don't move!");
					player2.sendMessage("Cancelled teleport, don't move!");
					cancel();
				}
				if (count == 0){
					player2.teleport(loc);
					cancel();
				}
			}

		}.runTaskTimer(plugin, 20, 20);
	}
	@EventHandler
	public void onQuit(PlayerQuitEvent event1) {
		Player player = event1.getPlayer();
		event1.setQuitMessage(null);
		if (getConfig().contains(player.getName())){
			String guild = getConfig().getString(player.getName() + ".Guild.Name");
			for (String key : getConfig().getConfigurationSection("Guilds." + guild + ".Players").getKeys(false)){
				if (Bukkit.getPlayer(key) != null){
					Player p = Bukkit.getPlayer(key);
					p.sendMessage("�3" + player.getName() + "�2 Has gone offline!");

				}

			}
		}
	}
}